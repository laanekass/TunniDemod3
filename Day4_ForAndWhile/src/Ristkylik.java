
public class Ristkylik {
	public static void main(String[] args) {
		int[][] ristkylikuMootudeTabel = { { 1, 0 }, { 2, 5 }, { 98, 75 } };

		for (int reaNr = 0; reaNr < ristkylikuMootudeTabel.length; reaNr++) {
			int esimeneKylg = 0;
			int teinekylg = 0;
			for (int veeruNr = 0; veeruNr < ristkylikuMootudeTabel[reaNr].length; veeruNr++) {
				if (veeruNr == 0) {
					esimeneKylg = ristkylikuMootudeTabel[reaNr][veeruNr];
				} else {
					teinekylg = ristkylikuMootudeTabel[reaNr][veeruNr];
				}
			}
			System.out.println("Ristkülikul külgedega " + esimeneKylg + " ja " + teinekylg + " on pindalaga "
					+ (esimeneKylg * teinekylg));
		}

		System.out.println(" --------------------------------------- ");

		for (int reaNr = 0; reaNr < ristkylikuMootudeTabel.length; reaNr++) {
			System.out.println("Ristkülikul külgedega " + ristkylikuMootudeTabel[reaNr][0] + " ja "
					+ ristkylikuMootudeTabel[reaNr][1] + " on pindalaga "
					+ (ristkylikuMootudeTabel[reaNr][0] * ristkylikuMootudeTabel[reaNr][1]));
		}

		System.out.println(" --------while------------------------------- ");
		int reaNr = 0;
		while (reaNr < ristkylikuMootudeTabel.length) {
			System.out.println("Ristkülikul külgedega " + ristkylikuMootudeTabel[reaNr][0] + " ja "
					+ ristkylikuMootudeTabel[reaNr][1] + " on pindalaga "
					+ (ristkylikuMootudeTabel[reaNr][0] * ristkylikuMootudeTabel[reaNr][1]));
			reaNr++;
		}

		System.out.println("-------for-each----------------------------------");
		for (int[] yheristkylikuMoodud : ristkylikuMootudeTabel) {
			System.out.println("Ristkülikul külgedega " + yheristkylikuMoodud[0] + " ja " + yheristkylikuMoodud[1]
					+ " on pindalaga " + (yheristkylikuMoodud[0] * yheristkylikuMoodud[1]));
			
			for(int ristkylikuKylg: yheristkylikuMoodud) {
				System.out.println("ristküliku külg on " + ristkylikuKylg);
			}
		}
	}

}
