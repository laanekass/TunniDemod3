package ee.bcs.koolitus.interfaces;

import java.text.DecimalFormat;

public class Kolmnurk implements Kujund {
	private double alusePikkus;
	private double korgusePikkus;

	@Override
	public double arvutaPindala() {
		return Double.valueOf(new DecimalFormat("#.##").format((alusePikkus * korgusePikkus) / 2));
	}

	public double getAlusePikkus() {
		return alusePikkus;
	}

	public Kolmnurk setAlusePikkus(double alusePikkus) {
		this.alusePikkus = alusePikkus;
		return this;
	}

	public double getKorgusePikkus() {
		return korgusePikkus;
	}

	public Kolmnurk setKorgusePikkus(double korgusePikkus) {
		this.korgusePikkus = korgusePikkus;
		return this;
	}

}
