package test.ee.bcs.koolitus.interfaces;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({RuutTest.class, KolmnurkTest.class})
public class AllTests {

}
