package test.ee.bcs.koolitus.interfaces;

import org.junit.Assert;
import org.junit.Test;

import ee.bcs.koolitus.interfaces.Kolmnurk;
import ee.bcs.koolitus.interfaces.Kujund;

public class KolmnurkTest {
	@Test
	public void testPindalaArvutamine() {
		Kujund kolmnurk = new Kolmnurk().setAlusePikkus(2).setKorgusePikkus(2);
		double pindala = kolmnurk.arvutaPindala();
		Assert.assertEquals(2.0, pindala, 0.0);
	}
}
