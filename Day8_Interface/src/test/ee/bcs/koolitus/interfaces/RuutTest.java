package test.ee.bcs.koolitus.interfaces;

import org.junit.Assert;
import org.junit.Test;

import ee.bcs.koolitus.interfaces.Kujund;
import ee.bcs.koolitus.interfaces.Ruut;

public class RuutTest {

		@Test
		public void testPindalaArvutamine() {
			Kujund ruut = new Ruut().setKyljePikkus(4);
			double pindala = ruut.arvutaPindala();
			Assert.assertEquals(16.0, pindala, 0.0);
		}
}
