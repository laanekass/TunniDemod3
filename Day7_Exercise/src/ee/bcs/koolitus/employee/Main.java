package ee.bcs.koolitus.employee;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {

	public static void changeSalary(Employee employee) {
		// employee.setSalary(employee.getSalary()
		// .add(BigDecimal.valueOf(100)));
		changeSalary(employee, BigDecimal.valueOf(100));
	}

	public static void changeSalary(Employee employee, BigDecimal salaryChange) {
		employee.setSalary(employee.getSalary().add(salaryChange));
	}

	public static void main(String[] args) throws ParseException {
		Employee employee = new Employee();
		employee.setSalary(BigDecimal.valueOf(1_000));
		changeSalary(employee);
		System.out.println(employee.getSalary());
		changeSalary(employee, BigDecimal.TEN);
		System.out.println(employee.getSalary());

		Employee employeeWithBigConstructor = new Employee("Mari", "", "Kask",
				new SimpleDateFormat("dd-MM-yyyy").parse("31-08-1992"),
				new SimpleDateFormat("dd-MM-yyyy").parse("02-02-2015"), null);

		Employee employeeWithBuilder = new Employee.EmployeeBuilder().firstName("Mati").middleName("").lastName("Tamm")
				.birthDate(new SimpleDateFormat("dd-MM-yyyy").parse("05-05-1982"))
				.startingDate(new SimpleDateFormat("dd-MM-yyyy").parse("31-08-2000"))
				.salary(null).build();
		
		System.out.println(new SimpleDateFormat("dd/MM/yyyy").format(employeeWithBuilder.getBirthDate()));
	}

}
