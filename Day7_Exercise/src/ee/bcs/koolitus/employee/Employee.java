package ee.bcs.koolitus.employee;

import java.math.BigDecimal;
import java.util.Date;

public class Employee {
	private String firstName;
	private String middleName;
	private String lastName;
	private Date birthDate;
	private Date startingDate;
	private BigDecimal salary;

	public Employee() {
		
	}
	
	public Employee(String firstName, String middleName, String lastName, Date birthDate, Date startingDate,
			BigDecimal salary) {
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		this.startingDate = startingDate;
		this.salary = salary;
	}	

	public String getFirstName() {
		return firstName;
	}

	public Employee setFirstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public String getMiddleName() {
		return middleName;
	}

	public Employee setMiddleName(String middleName) {
		this.middleName = middleName;
		return this;
	}

	public String getLastName() {
		return lastName;
	}

	public Employee setLastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public Employee setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
		return this;
	}

	public Date getStartingDate() {
		return startingDate;
	}

	public Employee setStartingDate(Date startingDate) {
		this.startingDate = startingDate;
		return this;
	}

	public BigDecimal getSalary() {
		return salary;
	}

	public Employee setSalary(BigDecimal salary) {
		this.salary = salary;
		return this;
	}
	
	public static class EmployeeBuilder {
		private String firstName;
		private String middleName;
		private String lastName;
		private Date birthDate;
		private Date startingDate;
		private BigDecimal salary;
		
		public EmployeeBuilder firstName (final String firstName) {
			this.firstName = firstName;
			return this;
		}
		
		public EmployeeBuilder middleName (final String middleName) {
			this.middleName = middleName;
			return this;
		}
		public EmployeeBuilder lastName (final String lastName) {
			this.lastName = lastName;
			return this;
		}
		public EmployeeBuilder birthDate (final Date birthDate) {
			this.birthDate = birthDate;
			return this;
		}
		public EmployeeBuilder startingDate (final Date startingDate) {
			this.startingDate = startingDate;
			return this;
		}
		public EmployeeBuilder salary (final BigDecimal salary) {
			this.salary = salary;
			return this;
		}
		
		public Employee build() {
			return new Employee(firstName, middleName, lastName, birthDate, startingDate, salary);
		}
		
	}
}
