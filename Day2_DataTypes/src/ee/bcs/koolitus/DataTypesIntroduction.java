package ee.bcs.koolitus;

import java.util.ArrayList;
import java.util.List;

public class DataTypesIntroduction {

	public static void main(String[] args) {
		byte esimeneByte = 127;
		byte teineByte = 16;

		Integer test = 10;

		double esimeneDouble = 5;

		System.out.println("esimene byte = " + (esimeneByte + teineByte + test));

		System.out.println(" double väärtus on " + (esimeneDouble + test));
		/**
		 * byte summa = (byte) (esimeneByte + teineByte); lause
		 * System.out.println("summa on " + summa);
		 */

		char esimeneChar = 'a';
		char teineChar = 'h';

		System.out.println(esimeneChar);
		System.out.println(teineChar);
		System.out.println("" + (esimeneChar - teineChar));

		System.out.println(esimeneChar == 'a');
		System.out.println(esimeneChar == 'k');

		System.out.println(test == 12);
		System.out.println(test == 10);

		System.out.println(esimeneChar == teineChar);
		char kolmasChar = 'a';
		System.out.println(esimeneChar == kolmasChar);

		int counter = 5;
		counter = counter + 1;
		System.out.println("counter = " + (counter = counter + 1));
		counter++;
		System.out.println("counter = " + (++counter));
		System.out.println(counter);

		int a = 5;
		int b = ++a;
		System.out.println("a = " + a + "; b = " + b);
		b = a++;
		System.out.println("a = " + a + "; b = " + b);

		a = 100;
		a += 5;
		System.out.println("a = " + a);
		a = a + 5;
		System.out.println("a = " + a);

		System.out.println(a % 2);
		System.out.println(a % 10);
		System.out.println(a % 3);

		String esimeneLause = "Jõmmu ei oska puu otsast alla tulla";
		System.out.println(esimeneLause);

		esimeneLause = esimeneLause + ", selle asemel hädaldab kõva häälega";
		System.out.println(esimeneLause);

		String eesnimi = "Henn";
		String perenimi = "Sarv";

		String koguNimi = eesnimi + " " + perenimi;
		System.out.println(koguNimi);
		System.out.println("Henn Sarv" == koguNimi);
		System.out.println("Henn Sarv".equals(koguNimi));

		String koheKoguNimi = "Henn Sarv";

		System.out.println(koheKoguNimi == koguNimi);
		System.out.println(koheKoguNimi.equals(koguNimi));

		String kymme = "10";
		int arvKymme = Integer.parseInt(kymme);
		System.out.println(arvKymme);

		String kymme2 = "20";
		double doubleArv = Double.parseDouble(kymme2);
		System.out.println(doubleArv);
		String textDoubleist = ((Double) doubleArv).toString();
		textDoubleist = "" + doubleArv;
	}

}
