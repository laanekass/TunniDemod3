package ee.bcs.koolitus.employeemanagement.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.employeemanagement.dao.Address;
import ee.bcs.koolitus.employeemanagement.dao.Human;
import ee.bcs.koolitus.employeemanagement.resource.HumanResource;

@Path("/humans")
public class HumanController {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Human> getAllHumans() {
		return HumanResource.getAllHumans();
	}

	@GET
	@Path("/{humanId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Human getHumanById(@PathParam("humanId") int humanId) {
		return HumanResource.getHumanById(humanId);
	}

	@GET
	@Path("/personalCode/{personalIdCode}")
	@Produces(MediaType.APPLICATION_JSON)
	public Human getHumanByPersonalCode(@PathParam("personalIdCode") String personalIdCode) {
		return HumanResource.getHumanByPersonalIdCode(personalIdCode);
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Human addNewHuman(Human human) {
		return HumanResource.addHuman(human);
	}

	@POST
	@Path("/{humanId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void addNewAddressToHuman(@PathParam("humanId") int humanId, Address address) {
		HumanResource.addNewAddressToHuman(humanId, address);
	}
	
	@POST
	@Path("/{humanId}/address/{addressId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void addAddressToHuman(@PathParam("humanId") int humanId, @PathParam("addressId") int addressId) {
		HumanResource.addAddressToHuman(humanId, addressId);
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateHuman(Human human) {
		HumanResource.updateHuman(human);
	}

	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	public void deleteHuman(Human human) {
		HumanResource.deleteHuman(human);
	}

	@DELETE
	@Path("/{humanId}")
	@Consumes(MediaType.APPLICATION_JSON)
	public void deleteHuman(@PathParam("humanId") int humanId) {
		HumanResource.deleteHumanById(humanId);
	}
	
	@DELETE
	@Path("/{humanId}/address/{addressId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void deleteAddressFromHuman(@PathParam("humanId") int humanId, @PathParam("addressId") int addressId) {
		HumanResource.deleteAddressFromHuman(humanId, addressId);
	}
}
