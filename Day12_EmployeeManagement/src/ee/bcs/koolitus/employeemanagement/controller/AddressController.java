package ee.bcs.koolitus.employeemanagement.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.employeemanagement.dao.Address;
import ee.bcs.koolitus.employeemanagement.resource.AddressResource;

@Path("/addresses")
public class AddressController {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Address> getAllAddresses() {
		return AddressResource.getAllAddresses();
	}

	@GET
	@Path("/{addressId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Address getAddressById(@PathParam("addressId") int addressId) {
		return AddressResource.getAddressById(addressId);
	}
	
	@GET
	@Path("/county/{countyName}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Address> getAddressByCountyName(@PathParam("countyName") String countyName) {
		return AddressResource.getAddressByCountyName(countyName);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Address addNewAddress(Address address) {
		return AddressResource.addAddress(address);
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateAddress(Address address) {
		AddressResource.updateAddress(address);
	}
	
	@DELETE
	@Path("/{addressId}")
	public void deleteAddressByAddresssId(@PathParam("addressId") int addressId) {
		AddressResource.deleteAddressByAddressId(addressId);
	}
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	public void deleteAddress (Address address) {
		AddressResource.deleteAddress(address);
	}
}
