package ee.bcs.koolitus.employeemanagement.resource;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.koolitus.employeemanagement.dao.Address;

public abstract class AddressResource {

	public static List<Address> getAllAddresses() {
		List<Address> addresses = new ArrayList<>();
		String sqlQuery = "SELECT * FROM address";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				addresses.add(new Address().setId(results.getInt("id")).setCounty(results.getString("county"))
						.setCity(results.getString("city")).setVillage(results.getString("village"))
						.setStreet(results.getString("street")).setHouseNo(results.getString("house_no"))
						.setFlatNo(results.getString("flat_no")).setFarmName(results.getString("farm_name")));
			}
		} catch (SQLException e) {
			System.out.println("Error on getting address set: " + e.getMessage());
		}

		return addresses;
	}

	public static Address getAddressById(int addressId) {
		Address address = null;
		String sqlQuery = "SELECT * FROM address WHERE id=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setInt(1, addressId);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				address = new Address().setId(results.getInt("id")).setCounty(results.getString("county"))
						.setCity(results.getString("city")).setVillage(results.getString("village"))
						.setStreet(results.getString("street")).setHouseNo(results.getString("house_no"))
						.setFlatNo(results.getString("flat_no")).setFarmName(results.getString("farm_name"));
			}
		} catch (SQLException e) {
			System.out.println("Error on getting address set: " + e.getMessage());
		}

		return address;
	}

	public static List<Address> getAddressByCountyName(String countyName) {
		List<Address> addresses = new ArrayList<>();
		String sqlQuery = "SELECT * FROM address WHERE UPPER(county) like UPPER(?)";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setString(1, countyName);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				Address address = new Address().setId(results.getInt("id")).setCounty(results.getString("county"))
						.setCity(results.getString("city")).setVillage(results.getString("village"))
						.setStreet(results.getString("street")).setHouseNo(results.getString("house_no"))
						.setFlatNo(results.getString("flat_no")).setFarmName(results.getString("farm_name"));
				addresses.add(address);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting address set: " + e.getMessage());
		}

		return addresses;
	}

	public static Address addAddress(Address address) {
		String sqlQuery = "INSERT INTO address (county, city, village, street, house_no, flat_no, farm_name) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?)";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery,
				Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1, address.getCounty());
			statement.setString(2, address.getCity());
			statement.setString(3, address.getVillage());
			statement.setString(4, address.getStreet());
			statement.setString(5, address.getHouseNo());
			statement.setString(6, address.getFlatNo());
			statement.setString(7, address.getFarmName());
			statement.executeUpdate();
			ResultSet resultSet = statement.getGeneratedKeys();
			while (resultSet.next()) {
				address.setId(resultSet.getInt(1));
			}
		} catch (SQLException e) {
			System.out.println("Error on adding address: " + e.getMessage());
		}

		return address;
	}

	public static void updateAddress(Address address) {
		String sqlQuery = "UPDATE address SET county=?, city=?, village=?, street=?, house_no=?, flat_no=?, farm_name=? WHERE id = ?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setString(1, address.getCounty());
			statement.setString(2, address.getCity());
			statement.setString(3, address.getVillage());
			statement.setString(4, address.getStreet());
			statement.setString(5, address.getHouseNo());
			statement.setString(6, address.getFlatNo());
			statement.setString(7, address.getFarmName());
			statement.setInt(8, address.getId());
			Integer code = statement.executeUpdate();
			if (code != 1) {
				throw new SQLException("Something went wrong on updating address");
			}
		} catch (SQLException e) {
			System.out.println("Error on updating address: " + e.getMessage());
		}
	}

	public static void deleteAddress(Address address) {
		deleteAddressByAddressId(address.getId());
	}

	public static void deleteAddressByAddressId(int addressId) {
		String sqlQuery = "DELETE from address WHERE id =?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setInt(1, addressId);
			Integer code = statement.executeUpdate();
			if (code != 1) {
				throw new SQLException("Something went wrong on deleting address");
			}
		} catch (SQLException e) {
			System.out.println("Error on deleting address: " + e.getMessage());
		}
	}
}
