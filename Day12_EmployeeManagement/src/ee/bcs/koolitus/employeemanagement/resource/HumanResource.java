package ee.bcs.koolitus.employeemanagement.resource;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.koolitus.employeemanagement.dao.Address;
import ee.bcs.koolitus.employeemanagement.dao.Human;

public abstract class HumanResource {

	public static List<Human> getAllHumans() {
		List<Human> humans = new ArrayList<>();
		String sqlQuery = "SELECT * FROM human";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				humans.add(new Human().setId(results.getInt("id"))
						.setPersonalIdCode(results.getString("personal_id_code"))
						.setFirstname(results.getString("firstname")).setLastname(results.getString("lastname"))
						.setBirthday(results.getDate("birthday")).setGender(results.getString("gender").charAt(0))
						.setAddress(AddressResource.getAddressById(results.getInt("address_id"))));
			}
		} catch (SQLException e) {
			System.out.println("Error on getting humans set: " + e.getMessage());
		}
		return humans;
	}

	public static Human getHumanById(int humanId) {
		Human human = null;
		String sqlQuery = "SELECT * FROM human WHERE id=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setInt(1, humanId);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				human = new Human().setId(results.getInt("id")).setPersonalIdCode(results.getString("personal_id_code"))
						.setFirstname(results.getString("firstname")).setLastname(results.getString("lastname"))
						.setBirthday(results.getDate("birthday")).setGender(results.getString("gender").charAt(0))
						.setAddress(AddressResource.getAddressById(results.getInt("address_id")));
			}
		} catch (SQLException e) {
			System.out.println("Error on getting human: " + e.getMessage());
		}
		return human;
	}

	public static Human getHumanByPersonalIdCode(String personalIdCode) {
		Human human = null;
		String sqlQuery = "SELECT * FROM human WHERE personal_id_code=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setString(1, personalIdCode);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				human = new Human().setId(results.getInt("id")).setPersonalIdCode(results.getString("personal_id_code"))
						.setFirstname(results.getString("firstname")).setLastname(results.getString("lastname"))
						.setBirthday(results.getDate("birthday")).setGender(results.getString("gender").charAt(0))
						.setAddress(AddressResource.getAddressById(results.getInt("address_id")));
			}
		} catch (SQLException e) {
			System.out.println("Error on getting human: " + e.getMessage());
		}
		return human;
	}

	public static Human addHuman(Human human) {
		String sqlQuery = "INSERT INTO human (personal_id_code, firstname, lastname, birthday, gender, address_id) "
				+ "VALUES (?, ?, ?, ?, ?, ?)";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery,
				Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1, human.getPersonalIdCode());
			statement.setString(2, human.getFirstname());
			statement.setString(3, human.getLastname());
			statement.setDate(4, javaDateToSqlDate(human.getBirthday()));
			statement.setString(5, String.valueOf(human.getGender()));
			statement.setInt(6, human.getAddress().getId());
			statement.executeUpdate();
			ResultSet resultSet = statement.getGeneratedKeys();
			while (resultSet.next()) {
				human.setId(resultSet.getInt(1));
			}
		} catch (SQLException e) {
			System.out.println("Error on adding address: " + e.getMessage());
		}
		return human;
	}

	public static void updateHuman(Human human) {
		String sqlQuery = "UPDATE human SET personal_id_code=?, firstname=?, lastname=?, birthday=?, gender=?, address_id=? WHERE id=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setString(1, human.getPersonalIdCode());
			statement.setString(2, human.getFirstname());
			statement.setString(3, human.getLastname());
			statement.setDate(4, javaDateToSqlDate(human.getBirthday()));
			statement.setString(5, String.valueOf(human.getGender()));
			statement.setInt(6, human.getAddress().getId());
			statement.setInt(7, human.getId());
			Integer code = statement.executeUpdate();
			if (code != 1) {
				throw new SQLException("Something went wrong on updating human");
			}
		} catch (SQLException e) {
			System.out.println("Error on updating human: " + e.getMessage());
		}
	}

	private static Date javaDateToSqlDate(java.util.Date date) {
		return new java.sql.Date(date.getTime());
	}

	public static void deleteHuman(Human human) {
		deleteHumanById(human.getId());
	}

	public static void deleteHumanById(int humanId) {
		String sqlQuery = "DELETE from human WHERE id =?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setInt(1, humanId);
			Integer code = statement.executeUpdate();
			if (code != 1) {
				throw new SQLException("Something went wrong on deleting human");
			}
		} catch (SQLException e) {
			System.out.println("Error on updating human: " + e.getMessage());
		}
	}

	public static void addNewAddressToHuman(int humanId, Address address) {
		addAddressToHuman(humanId, AddressResource.addAddress(address).getId());
	}

	public static void addAddressToHuman(int humanId, int addressId) {
		String sqlQuery = "UPDATE human SET address_id=? WHERE id=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setInt(1, addressId);
			statement.setInt(1, humanId);
			Integer code = statement.executeUpdate();
			if (code != 1) {
				throw new SQLException("Something went wrong on adding human address");
			}
		} catch (SQLException e) {
			System.out.println("Error on adding new address to human: " + e.getMessage());
		}

	}

	public static void deleteAddressFromHuman(int humanId, int addressId) {
		String sqlQuery = "UPDATE human SET address_id=? WHERE id=? and address_id=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setNull(1, java.sql.Types.NULL);
			
			statement.setInt(1, humanId);
			Integer code = statement.executeUpdate();
			if (code != 1) {
				throw new SQLException("Something went wrong on adding human address");
			}
		} catch (SQLException e) {
			System.out.println("Error on adding new address to human: " + e.getMessage());
		}

	}
}
