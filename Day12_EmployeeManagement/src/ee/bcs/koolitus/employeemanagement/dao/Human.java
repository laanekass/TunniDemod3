package ee.bcs.koolitus.employeemanagement.dao;

import java.util.Date;

public class Human {
	private int id;
	private String personalIdCode;
	private String firstname;
	private String lastname;
	private Date birthday;
	private char gender;
	private Address address;

	public int getId() {
		return id;
	}

	public Human setId(int id) {
		this.id = id;
		return this;
	}

	public String getPersonalIdCode() {
		return personalIdCode;
	}

	public Human setPersonalIdCode(String personalIdCode) {
		this.personalIdCode = personalIdCode;
		return this;
	}

	public String getFirstname() {
		return firstname;
	}

	public Human setFirstname(String firstname) {
		this.firstname = firstname;
		return this;
	}

	public String getLastname() {
		return lastname;
	}

	public Human setLastname(String lastname) {
		this.lastname = lastname;
		return this;
	}

	public Date getBirthday() {
		return birthday;
	}

	public Human setBirthday(Date birthday) {
		this.birthday = birthday;
		return this;
	}

	public char getGender() {
		return gender;
	}

	public Human setGender(char gender) {
		this.gender = gender;
		return this;
	}

	public Address getAddress() {
		return address;
	}

	public Human setAddress(Address address) {
		this.address = address;
		return this;
	}

	@Override
	public String toString() {
		return "Human[id=" + id + ", personalCode=" + personalIdCode + ", firstname=" + firstname + ", lastname="
				+ lastname + ", birthday=" + birthday + ", gender" + gender + ", addressId=" + address + "]";
	}

}
