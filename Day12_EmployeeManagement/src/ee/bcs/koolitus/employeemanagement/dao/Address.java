package ee.bcs.koolitus.employeemanagement.dao;

public class Address {
	private int id;
	private String county;
	private String city;
	private String village;
	private String street;
	private String houseNo;
	private String flatNo;
	private String farmName;

	public int getId() {
		return id;
	}

	public Address setId(int id) {
		this.id = id;
		return this;
	}

	public String getCounty() {
		return county;
	}

	public Address setCounty(String county) {
		this.county = county;
		return this;
	}

	public String getCity() {
		return city;
	}

	public Address setCity(String city) {
		this.city = city;
		return this;
	}

	public String getVillage() {
		return village;
	}

	public Address setVillage(String village) {
		this.village = village;
		return this;
	}

	public String getStreet() {
		return street;
	}

	public Address setStreet(String street) {
		this.street = street;
		return this;
	}

	public String getHouseNo() {
		return houseNo;
	}

	public Address setHouseNo(String houseNo) {
		this.houseNo = houseNo;
		return this;
	}

	public String getFlatNo() {
		return flatNo;
	}

	public Address setFlatNo(String flatNo) {
		this.flatNo = flatNo;
		return this;
	}

	public String getFarmName() {
		return farmName;
	}

	public Address setFarmName(String farmName) {
		this.farmName = farmName;
		return this;
	}

	@Override
	public String toString() {
		return "Address[id=" + id + ", county=" + county + ", city=" + city + ", village=" + village + ", street="
				+ street + ", houseNo=" + houseNo + ", flatNo=" + flatNo + ", farmName=" + farmName + "]";
	}
}
