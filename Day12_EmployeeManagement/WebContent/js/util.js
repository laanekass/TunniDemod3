function checkIfNullOrUndefined(value) {
	if (value != null && value != undefined && value != "null"
			&& value != "undefined") {
		return value;
	} else {
		return "";
	}
}

function showHideElementById(elementId) {
	var x = document.getElementById(elementId);
	if (x.style.display === "none") {
		x.style.display = "block";
	} else {
		x.style.display = "none";
	}
}