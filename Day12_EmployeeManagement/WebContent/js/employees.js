var allPeople;
function getAllPeople() {
	$.getJSON("http://localhost:8080/EmployeeManagement/rest/humans", function(
			people) {
		allPeople = people;
		fillPeopleTable(allPeople);
	});
}
getAllPeople();

function fillPeopleTable(people) {
	var tableBody = document.getElementById("peopleTableBody");
	var tableContent = "";
	for (var i = 0; i < people.length; i++) {
		tableContent = tableContent
				+ "<tr><td>"
				+ checkIfNullOrUndefined(people[i].firstname)
				+ "</td><td>"
				+ checkIfNullOrUndefined(people[i].lastname)
				+ "</td><td>"
				+ checkIfNullOrUndefined(people[i].PersonalIdCode)
				+ "</td><td>"
				+ checkIfNullOrUndefined(people[i].gender)
				+ "</td><td>"
				+ checkIfNullOrUndefined(people[i].birthday)
				+ "</td><td>"
				+ addressToString(checkIfNullOrUndefined(people[i].address))
				+ "</td><td><button type='button' class='btn btn-light' onClick='deletePerson("
				+ people[i].id
				+ ")'>Delete</button>"
				+ "<button type='button' class='btn btn-light' onClick='modifyPerson("
				+ people[i].id + ")'>Update</button></td></tr>";
	}
	tableBody.innerHTML = tableContent;
}

var allAddresses;
function getAllAddresses() {
	$.getJSON("http://localhost:8080/EmployeeManagement/rest/addresses",
			function(addresses) {
				allAddresses = addresses;
				fillAddressesSelect();
			});
}
getAllAddresses();

function fillAddressesSelect() {
	var addressSelectOptions = "";
	for (var i = 0; i < allAddresses.length; i++) {
		addressSelectOptions = addressSelectOptions + "<option value='"
				+ allAddresses[i].id + "'>" + addressToString(allAddresses[i])
				+ "</option>";
	}
	document.getElementById("addressSelect").innerHTML = addressSelectOptions;
}

function addressToString(address) {
	var addressString = address == "" ? ""
			: (addElementIfDefined(address.county)
					+ addElementIfDefined(address.city)
					+ addElementIfDefined(address.village)
					+ addElementIfDefined(address.street)
					+ addElementIfDefined(address.houseNo) + addElementIfDefined(address.flatNo));

	return removeTrailingComma(addressString);
}

function addElementIfDefined(addressElement) {
	return checkIfNullOrUndefined(addressElement) == "" ? "" : addressElement
			+ ", ";
}

function removeTrailingComma(addressString) {
	return addressString != ""
			&& addressString.trim()[addressString.trim().length - 1] == "," ? addressString
			.trim().substring(0, addressString.trim().length - 1)
			: addressString;
}

function saveNewPerson() {

}

function deletePerson(personId) {

}

function modifyPerson(personId) {

}