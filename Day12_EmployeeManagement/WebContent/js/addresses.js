var dataTable;
showHideElementById("newAddressBlock");
showHideElementById("modifyAddressBlock");

var allAddresses;
function getAllAddresses() {
	$.getJSON("http://localhost:8080/EmployeeManagement/rest/addresses",
			function(addresses) {
				allAddresses = addresses;
				fillAddressesTable(addresses)
			});
}
getAllAddresses();

function fillAddressesTable(addresses) {
	if (dataTable != undefined) {
		dataTable.clear();
		dataTable.destroy();
	}
	var tableBody = document.getElementById("addressTableBody");
	var tableContent = "";
	for (var i = 0; i < addresses.length; i++) {
		tableContent = tableContent
				+ "<tr><td>"
				+ checkIfNullOrUndefined(addresses[i].id)
				+ "</td><td>"
				+ checkIfNullOrUndefined(addresses[i].county)
				+ "</td><td>"
				+ checkIfNullOrUndefined(addresses[i].city)
				+ "</td><td>"
				+ checkIfNullOrUndefined(addresses[i].village)
				+ "</td><td>"
				+ checkIfNullOrUndefined(addresses[i].street)
				+ "</td><td>"
				+ checkIfNullOrUndefined(addresses[i].houseNo)
				+ "</td><td>"
				+ checkIfNullOrUndefined(addresses[i].flatno)
				+ "</td><td>"
				+ checkIfNullOrUndefined(addresses[i].farmName)
				+ "</td><td><button type='button' class='btn btn-light' onClick='deleteAddress("
				+ addresses[i].id
				+ ")'>Delete</button>"
				+ "<button type='button' class='btn btn-light' onClick='fillModifyForm("
				+ addresses[i].id + ")'>Update</button></td></tr>";
	}
	tableBody.innerHTML = tableContent;
	dataTable = $("#addressTable").DataTable();
}

function showAddAddress() {
	showHideElementById("newAddressBlock");
}

function addNewAddress() {
	var newAddressJson = {
		"county" : document.getElementById("county").value,
		"city" : document.getElementById("city").value,
		"street" : document.getElementById("street").value
	}
	console.log(newAddressJson);
	var newAddress = JSON.stringify(newAddressJson);

	$.ajax({
		url : "http://localhost:8080/EmployeeManagement/rest/addresses",
		type : "POST",
		data : newAddress,
		contentType : "application/json; charset=utf-8",
		success : function() {
			showHideElementById("newAddressBlock");
			getAllAddresses();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}

function deleteAddress(addressId) {
	var deleteAddressJson = {
		"id" : addressId
	};
	$.ajax({
		url : "http://localhost:8080/EmployeeManagement/rest/addresses",
		type : "DELETE",
		data : JSON.stringify(deleteAddressJson),
		contentType : "application/json; charset=utf-8",
		success : function() {
			getAllAddresses();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}

function fillModifyForm(addressId) {
	for (var i = 0; i < allAddresses.length; i++) {
		if (allAddresses[i].id == addressId) {
			document.getElementById("idModify").value = addressId;
			document.getElementById("countyModify").value = allAddresses[i].county;
			document.getElementById("cityModify").value = allAddresses[i].city;
			document.getElementById("streetModify").value = allAddresses[i].street;
		}
	}
	showHideElementById("modifyAddressBlock");
}

function changeAddress() {
	var modifyAddressJson = {
		"id" : document.getElementById("idModify").value,
		"county" : document.getElementById("countyModify").value,
		"city" : document.getElementById("cityModify").value,
		"street" : document.getElementById("streetModify").value
	}
	console.log(modifyAddressJson);
	var modifyAddress = JSON.stringify(modifyAddressJson);

	$.ajax({
		url : "http://localhost:8080/EmployeeManagement/rest/addresses",
		type : "PUT",
		data : modifyAddress,
		contentType : "application/json; charset=utf-8",
		success : function() {
			showHideElementById("modifyAddressBlock");
			getAllAddresses();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log(textStatus);
		}
	});
}

function getAddressesByCounty() {
	var selectedCounty = document.getElementById("countySelect").value;
	if (selectedCounty != "") {
		$.getJSON(
				"http://localhost:8080/EmployeeManagement/rest/addresses/county/"
						+ selectedCounty, function(addresses) {
					allAddresses = addresses;
					fillAddressesTable(addresses)
				});
	}else {
		getAllAddresses();
	}
}
