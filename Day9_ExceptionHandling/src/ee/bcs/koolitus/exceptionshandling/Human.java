package ee.bcs.koolitus.exceptionshandling;

public class Human {
	private String name;

	public String getName() {
		return name;
	}

	public Human setName(String name) throws NameTooShortException {
		if ( name.length() <2 ) {
			throw new NameTooShortException("Human needs name that is at least two characters long");
		}
		this.name = name;
		return this;
	}
	
}
