package ee.bcs.koolitus.exceptionshandling;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		try (FileWriter writer = new FileWriter(new File("outputResults.txt"))){			
			int i = 1;
			while (i <= 5) {
				writer.append("There are " + i + " eggs in the bird nest\n");
				i++;
			}
			writer.append("Chicks have hatched, there is no eggs anymore");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("There is an error on writing to file outputResults.txt: " + e.getMessage());
		}
		
		try {
			Human human1 = new Human().setName("d");
			System.out.println("human name is " + human1.getName());
		} catch (NameTooShortException e) {
			System.out.println("Exception occured on setting human name: " + e.getMessage());
		}
		
	}

}
