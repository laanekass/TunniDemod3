package ee.bcs.koolitus.exceptionshandling;

public class NameTooShortException extends Exception{

	public NameTooShortException(String message) {
		super(message);
	}
}
