package ee.bcs.koolitus.inheritence;

import java.util.ArrayList;
import java.util.List;

public class Opetaja extends Inimene {
	private List<String> ained = new ArrayList<>();

	public Opetaja(String nimi, int vanus) {
		super(nimi, vanus);
	}

	public List<String> getAined() {
		return ained;
	}

	public Opetaja setAined(List<String> ained) {
		this.ained = ained;
		return this;
	}

	public Opetaja addNewAine(String aine) {
		ained.add(aine);
		return this;
	}

	@Override
	public String toString() {
		return "Õpetaja[nimi=" + getNimi() + ", vanus=" + getVanus() + 
				", ained=" + ained + "]";
	}
}
