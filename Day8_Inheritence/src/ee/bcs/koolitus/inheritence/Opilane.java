package ee.bcs.koolitus.inheritence;

public class Opilane extends Inimene {
	private int klass;

	// public Opilane() {
	// super(null, 0); //päris projektides ei ole soovitatav kasutada
	// }

	public Opilane(String nimi, int vanus) {
		this(nimi, vanus, -1);
	}
	
	public Opilane(String nimi, int vanus, int klass) {
		super(nimi, vanus);
		this.klass=klass;
	}

	public int getKlass() {
		return klass;
	}

	public Opilane setKlass(int klass) {
		this.klass = klass;
		return this;
	}

	@Override
	public String toString() {
		// return super.toString() + ". õpilane [klass=" + klass + "]";
		return "Õpilane[nimi=" + getNimi() + ", vanus= " + getVanus() + ", klass=" + klass + "]";
	}

}
