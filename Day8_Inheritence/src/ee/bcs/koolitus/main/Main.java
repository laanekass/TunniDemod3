package ee.bcs.koolitus.main;

import java.util.ArrayList;
import java.util.List;

import ee.bcs.koolitus.inheritence.Inimene;
import ee.bcs.koolitus.inheritence.Opetaja;
import ee.bcs.koolitus.inheritence.Opilane;

public class Main {

	public static void main(String[] args) {
		Inimene kati = new Inimene("Kati Kaasik", 25);
		System.out.println(kati);

		Opilane opilaneMati = (Opilane) new Opilane("Mati Tamm", 7).setKlass(1);
		System.out.println(opilaneMati);

		Opilane opilaneMari = new Opilane("Mari Maasikas", 10, 4);
		System.out.println(opilaneMari);

		Opetaja opetajaJyri = new Opetaja("Jüri Juurikas", 38);
		System.out.println(opetajaJyri);
		opetajaJyri.addNewAine("Matemaatika").addNewAine("Muusika Ajalugu");
		System.out.println(opetajaJyri);

		opetajaJyri.getAined().add("Ajalugu");
		System.out.println(opetajaJyri);

		Inimene inimene = new Opetaja("Kaarel Kuusk", 40);
		((Opetaja)inimene).getAined();
		
		List<Inimene> inimesed = new ArrayList<>();
		inimesed.add(inimene);
		inimesed.add(opetajaJyri);
		inimesed.add(opilaneMari);

	}

}
