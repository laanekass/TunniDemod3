
public class ExWithSwitch {

	public static void main(String[] args) {
		String lightColor = "Green";

		switch (lightColor.toLowerCase()) {
		case "green":
			System.out.println("Driver can drive a car.");
			break;
		case "yellow":
			System.out.println("Driver has to be ready to stop the car or to start driving.");
			break;
		case "red":
			System.out.println("Driver has to stop car and wait for green light.");
			break;
		default:
			System.out.println("You should not drive in disco bar!");
		}
		
		String s = new String("hjfhsdjk");
	}
}
