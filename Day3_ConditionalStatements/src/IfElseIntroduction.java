
public class IfElseIntroduction {

	public static void main(String[] args) {
		int a = 7;
		int b = -5;

		if (a >= 0 && b >= 0) {
			int vastus = a * b;
			System.out.println("a * b  = " + vastus);
			if (a > 6 && b < 6) {
				System.out.println("ahaa");
			}
		} else if (a == 5) {
			System.out.println("ei ole vaja korrutada");
		} else {
			System.out.println("ei olnud sobivat tingimust");
		}

		String antudNimi = "Mari";
		final String MINU_NIMI = "Kati";
		boolean onMinuNimi = (antudNimi.equals(MINU_NIMI)) ? true : false;

		System.out.println("Kas minu nimi on " + antudNimi + "? Vastus: " + onMinuNimi);
		
		boolean isDoorOpen = false;
		String uks = (isDoorOpen) ? "Uks on lahti": "Uks on kinni";
		System.out.println(uks);

	}

}
