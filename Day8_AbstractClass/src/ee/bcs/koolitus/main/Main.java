package ee.bcs.koolitus.main;

import ee.bcs.koolitus.abstractclass.Inimene;
import ee.bcs.koolitus.abstractclass.Koristaja;
import ee.bcs.koolitus.abstractclass.Opetaja;
import ee.bcs.koolitus.abstractclass.Opilane;
import ee.bcs.koolitus.abstractclass.Tootaja;

public class Main {

	public static void main(String[] args) {
		Inimene inimene1 = new Opilane();
		inimene1.setNimi("Mati");
		inimene1.soomine();
		
		Opilane opilane = new Opilane();
		opilane.setNimi("Kati");
		opilane.soomine();
		
		Inimene opetaja = new Opetaja().setNimi("Aadu");
		opetaja.soomine();
		
		Tootaja opetaja2 = new Opetaja();
		opetaja2.setNimi("Priidu");
		opetaja2.soomine();
		
		Opetaja opetaja3 = new Opetaja();
		opetaja3.soomine();
		
		Koristaja koristaja = new Koristaja();
		koristaja.soomine();
	}

}
