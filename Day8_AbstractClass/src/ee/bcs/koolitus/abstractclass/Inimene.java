package ee.bcs.koolitus.abstractclass;

public abstract class Inimene {
	private String nimi;
	
	public abstract void soomine();

	public String getNimi() {
		return nimi;
	}

	public Inimene setNimi(String nimi) {
		this.nimi = nimi;
		return this;
	}

}
