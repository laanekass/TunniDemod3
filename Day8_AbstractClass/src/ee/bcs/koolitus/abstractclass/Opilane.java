package ee.bcs.koolitus.abstractclass;

public class Opilane extends Inimene {

	@Override
	public void soomine() {
		System.out.println(getNimi() + ", võta söögiriistad, ja hakka nendega sööma");
	}

}
