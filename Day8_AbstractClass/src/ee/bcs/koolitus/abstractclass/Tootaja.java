package ee.bcs.koolitus.abstractclass;

public abstract class Tootaja extends Inimene{
	@Override
	public void soomine() {
		System.out.println("Söön siis kui aega on");
	}
}
