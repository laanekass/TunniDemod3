package ee.bcs.koolitus.auto;

public class Auto {
	private int usteArv;
	int kohtadeArv;
	protected Mootor mootor;
	public int id;
	private static int counter=1;

	public Auto() {
		setIdAndCounter();
	}
	
	public Auto(int usteArv, Mootor mootor) {
		this();
		this.usteArv = usteArv;
		this.mootor = mootor;
	}

	private void setIdAndCounter() {
		id = counter;
		counter++;
	}	

	public int arvutaKindlustus(String kasutajaNimi, int periood) {
		if (periood <= 0 || kasutajaNimi.equals("")) {
			return 0;
		} 			
		int summa = arvutaKindlustus(periood) + 10;
		if (kasutajaNimi.equals("Mari")) {
			summa += 15;
		} else if (kasutajaNimi.equals("Mati")) {
			summa += 45;
		}
		return summa;		
	}

	public int arvutaKindlustus(int vanus, int periood) {
		return (vanus < 26) ? arvutaKindlustus(periood) * 2 : arvutaKindlustus(periood);
	}

	public int arvutaKindlustus(int periood) {
		if (periood <= 0) {
			return 0;
		}
		return periood * 10;

		// variant2
		// if (periood <= 0) {
		// return 0;
		// } else {
		// return periood * 10;
		// }
	}

	public int getId() {
		return id;
	}

	public static int getCounter() {
		return counter;
	}

	public int getUsteArv() {
		return usteArv;
	}

	public Auto setUsteArv(int usteArv) {
		this.usteArv = usteArv;
		return this;
	}

	public int getKohtadeArv() {
		return kohtadeArv;
	}

	public Auto setKohtadeArv(int kohtadeArv) {
		this.kohtadeArv = kohtadeArv;
		return this;
	}

	public Mootor getMootor() {
		return mootor;
	}

	public Auto setMootor(Mootor mootor) {
		this.mootor = mootor;
		return this;
	}

	@Override
	public String toString() {
		return "Auto[ustearv=" + usteArv + ", kohtadeArv=" + kohtadeArv + ", mootor=" + mootor + "]";
	}

}
