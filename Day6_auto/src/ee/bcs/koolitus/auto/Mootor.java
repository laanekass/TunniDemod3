package ee.bcs.koolitus.auto;

public class Mootor {
	private int voimsus;
	private KytuseTyyp kytuseTyyp;

	public int getVoimsus() {
		return voimsus;
	}

	public void setVoimsus(int uusVoimsus) {
		this.voimsus = uusVoimsus;
	}

	public KytuseTyyp getKytuseTyyp() {
		return kytuseTyyp;
	}

	public void setKytuseTyyp(KytuseTyyp kytuseTyyp) {
		this.kytuseTyyp = kytuseTyyp;
	}

	@Override
	public String toString() {
		return "Mootor: võimsus=" + voimsus + "; kütusetüüp=" + kytuseTyyp;
	}
}
