package ee.bcs.koolitus.auto;

public class Main {

	public static int factorialCalculation(int givenNumber) {
		int factorial = givenNumber;
		if (givenNumber <= 1) {
			return factorial;
		} else {
			return factorialCalculation(givenNumber - 1) * givenNumber;
		}
	}

	public static String leiaSumma(String miskiTekst, int... arvud) {
		int summa = 0;
		for (int arv : arvud) {
			summa += arv;
		}
		return miskiTekst + summa;
	}

	public static void main(String[] args) {
		System.out.println(leiaSumma("Summa on: ", 1, 5, 8));

		System.out.println(leiaSumma("Nüüd on summa:", 1, 5, 8, 10, 25));

		System.out.println(factorialCalculation(5));

		Mootor mootor1 = new Mootor();
		mootor1.setVoimsus(84);
		mootor1.setKytuseTyyp(KytuseTyyp.BENSIIN);
		int mootor1Voimsus = mootor1.getVoimsus();
		System.out.println("Mootoril on võimsus " + mootor1Voimsus + "KW, kütusetüüp on " + mootor1.getKytuseTyyp());
		System.out.println("Mootor > " + mootor1);

		Auto autoVolvo = new Auto();
		autoVolvo.setKohtadeArv(5);
		autoVolvo.setMootor(mootor1);
		autoVolvo.setUsteArv(5);

		System.out.println("esimese auto id = " + autoVolvo.getId() + 
				" ja counter = " + Auto.getCounter());

		System.out.println(autoVolvo);
		System.out.println(autoVolvo.getMootor().getKytuseTyyp());

		Mootor mootor2 = new Mootor();
		mootor2.setVoimsus(125);
		mootor2.setKytuseTyyp(KytuseTyyp.DIISEL);

		Auto autoŠkoda = new Auto().setKohtadeArv(2).setUsteArv(3).setMootor(mootor2);
		System.out.println(autoŠkoda);
		
		System.out.println("Teise auto id = " + autoŠkoda.getId() + 
				" ja counter = " + Auto.getCounter());

		Mootor mootor3 = new Mootor();
		mootor3.setVoimsus(64);
		mootor3.setKytuseTyyp(KytuseTyyp.BENSIIN);

		Auto autoYaris = new Auto(5, mootor3).setKohtadeArv(5);
		System.out.println(autoYaris);
		
		System.out.println("esimese auto id = " + autoVolvo.getId() + 
				" ja counter = " + Auto.getCounter());
		System.out.println("Teise auto id = " + autoŠkoda.getId() + 
				" ja counter = " + Auto.getCounter());
		System.out.println("Kolmanda auto id = " + autoYaris.getId() + 
				" ja counter = " + Auto.getCounter());

		int a = autoYaris.arvutaKindlustus("Mati", 10);
		System.out.println("vanuse järgi:" + autoYaris.arvutaKindlustus(28, 10));
		System.out.println("Nime järgi: " + autoYaris.arvutaKindlustus("Mati", 10));
		
		
	}

}
