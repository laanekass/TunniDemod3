package ee.bcs.koolitus.car;

import java.awt.Color;

public class Car implements Comparable<Car> {
	private String registrationNo;
	private Color color;
	private String vinCode;
	private int numberOfDoors;

	public Car() {

	}

	public Car(String registrationNo, Color color, String vinCode, int numberOfDoors) {
		this.registrationNo = registrationNo;
		this.color = color;
		this.vinCode = vinCode;
		this.numberOfDoors = numberOfDoors;
	}

	// @Override
	// public boolean equals(Object obj) {
	// if (this == obj) {
	// return true;
	// }
	// if (obj == null) {
	// return false;
	// }
	// if (getClass() != obj.getClass()) {
	// return false;
	// }
	//
	// final Car other = (Car) obj;
	// boolean variablesEqual = this.registrationNo.equals(other.registrationNo) &&
	// this.color.equals(other.color)
	// && this.vinCode.equals(other.vinCode) && this.numberOfDoors ==
	// other.numberOfDoors;
	// return variablesEqual;
	// }
	//
	// @Override
	// public int hashCode() {
	// int hash = 89;
	// hash = 73 * hash + registrationNo.hashCode();
	// hash = 73 * hash + color.hashCode();
	// hash = 73 * hash + vinCode.hashCode();
	// hash = 73 * hash + numberOfDoors;
	// //alternatiiv eelmisele reale
	// //hash = 73 * hash + Integer.valueOf(numberOfDoors).hashCode();
	// return hash;
	// }

	public String getRegistrationNo() {
		return registrationNo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + numberOfDoors;
		result = prime * result + ((registrationNo == null) ? 0 : registrationNo.hashCode());
		result = prime * result + ((vinCode == null) ? 0 : vinCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Car other = (Car) obj;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		if (numberOfDoors != other.numberOfDoors)
			return false;
		if (registrationNo == null) {
			if (other.registrationNo != null)
				return false;
		} else if (!registrationNo.equals(other.registrationNo))
			return false;
		if (vinCode == null) {
			if (other.vinCode != null)
				return false;
		} else if (!vinCode.equals(other.vinCode))
			return false;
		return true;
	}

	public Car setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
		return this;
	}

	public Color getColor() {
		return color;
	}

	public Car setColor(Color color) {
		this.color = color;
		return this;
	}

	public String getVinCode() {
		return vinCode;
	}

	public Car setVinCode(String vinCode) {
		this.vinCode = vinCode;
		return this;
	}

	public int getNumberOfDoors() {
		return numberOfDoors;
	}

	public Car setNumberOfDoors(int numberOfDoors) {
		this.numberOfDoors = numberOfDoors;
		return this;
	}

	@Override
	public String toString() {
		return "Car [registrationNo=" + registrationNo + ", color=" + color + ", vinCode=" + vinCode + ", numberOfDoors"
				+ numberOfDoors + "]";
	}

	@Override
	public int compareTo(Car otherCar) {
		final int VORDNE = 0;
		final int VAIKSEM = -1;
		final int SUUREM = 1;
		if (this.equals(otherCar)) {			
			return 0;
		} else if (this.vinCode.compareTo(otherCar.vinCode) == VORDNE) {
			if (this.registrationNo.compareTo(otherCar.registrationNo) == VORDNE) {
				if (this.color.toString().compareTo(otherCar.color.toString()) == VORDNE) {
					return (this.numberOfDoors < otherCar.numberOfDoors) ? VAIKSEM
							: ((this.numberOfDoors > otherCar.numberOfDoors) ? SUUREM : VORDNE);
				} else {
					return this.color.toString().compareTo(otherCar.color.toString());
				}
			} else {
				return this.registrationNo.compareTo(otherCar.registrationNo);
			}
		} else {
			return this.vinCode.compareTo(otherCar.vinCode);
		}
	}

}
