package ee.bcs.koolitus.main;

import java.awt.Color;
import java.util.Set;
import java.util.TreeSet;

import ee.bcs.koolitus.car.Car;

public class Main {

	public static void main(String[] args) {
		Set<Car> allCars = new TreeSet<>();
		Car volvo = new Car("123ABC", Color.black, "asd763478236", 5);
		Car volvo2 = new Car("123ABC", Color.black, "asd763478236", 5);
		Car audi = new Car("567ABC", Color.red, "asd763jkdfk236", 5);
		Car bmw = new Car("890ABC", Color.blue, "ah9853jkdfk236", 3);
		allCars.add(bmw);
		allCars.add(volvo2);
		allCars.add(volvo);
		allCars.add(audi);
		
		System.out.println("volvo=volvo2? - " + volvo.equals(volvo2));
		System.out.println("volvo=audi? - " + volvo.equals(audi));
		System.out.println("bmw=volvo2? - " + bmw.equals(volvo2));
		System.out.println("audi=bmw? - " + audi.equals(bmw));
		
		for(Car car: allCars) {
			System.out.println(car);
		}

		System.out.println(audi.compareTo(bmw));
	}

}
