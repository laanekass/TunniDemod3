import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RistkylikListiga {

	public static void main(String[] args) {
		List<List<Double>> ristkylikuteMoodud = new ArrayList<>();
		ristkylikuteMoodud
				.addAll(Arrays.asList(Arrays.asList(3.5, 8.0), Arrays.asList(0.5, 2.0), 
						Arrays.asList(50.0, 8.5)));
		double summa = 0;
		for (List<Double> yheRistkylikuMoodud : ristkylikuteMoodud) {
			summa+=yheRistkylikuMoodud.get(0) * yheRistkylikuMoodud.get(1);
			System.out.println("Ristküliku pindala on " + (yheRistkylikuMoodud.get(0) 
					* yheRistkylikuMoodud.get(1)));
		}
		System.out.println("Kõigi ristkülikute pindalade summa on " + summa + " (ü2)");
		
		double summa2 = 0;
		for (int i= 0;i< ristkylikuteMoodud.size();i++) {
			summa2+=ristkylikuteMoodud.get(i).get(0) * ristkylikuteMoodud.get(i).get(1);
			System.out.println("Ristküliku pindala on " + (ristkylikuteMoodud.get(i).get(0) 
					* ristkylikuteMoodud.get(i).get(1)));
		}
		System.out.println("Kõigi ristkülikute pindalade summa on " + summa2 + " (ü2)");
	}
}
