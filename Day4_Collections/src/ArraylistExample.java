import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArraylistExample {

	public static void main(String[] args) {
		List<Integer> arvudeLoetelu = new ArrayList<>();
		arvudeLoetelu.add(7);
		arvudeLoetelu.add(15);
		arvudeLoetelu.add(7);
		arvudeLoetelu.add(-25);
		arvudeLoetelu.add(3678);
		arvudeLoetelu.addAll(Arrays.asList(12, 18, -325));

		System.out.println(arvudeLoetelu);
		arvudeLoetelu.add(3, 92);
		System.out.println(arvudeLoetelu);
		arvudeLoetelu.addAll(1, Arrays.asList(12, 18, -325));
		System.out.println(arvudeLoetelu);

		System.out.println("Listi suurus on: " + arvudeLoetelu.size());
		
		arvudeLoetelu.remove(2);
		System.out.println(arvudeLoetelu);
		System.out.println("Listi suurus on: " + arvudeLoetelu.size());
		
		arvudeLoetelu.removeAll(Arrays.asList(7,92));
		System.out.println(arvudeLoetelu);
		System.out.println("Listi suurus on: " + arvudeLoetelu.size());
		
		arvudeLoetelu.set(3, 1025);
		System.out.println(arvudeLoetelu);
		System.out.println("Listi suurus on: " + arvudeLoetelu.size());

		int summa = 0;
		//siia summa arvudest arvutamine
		for(int i = 0; i<arvudeLoetelu.size();i++) {
			summa += arvudeLoetelu.get(i);
		}
		System.out.println("Listis olevate arvude summa on: " + summa);
		
		summa = 0;
		//siia summa arvudest arvutamine
		for(Integer arv: arvudeLoetelu) {
			summa += arv;
		}
		System.out.println("Listis olevate arvude summa on: " + summa);
		
		int sum = arvudeLoetelu.stream().reduce(0,  (x,y) ->x+y);
		System.out.println("Listis olevate arvude summa on: " + sum);
		
		int sum2 = arvudeLoetelu.stream().mapToInt(Integer::intValue).sum();
		System.out.println("Listis olevate arvude summa on: " + sum2);
	}
}
