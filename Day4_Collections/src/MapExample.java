import java.util.HashMap;
import java.util.Map;

public class MapExample {
	public static void main(String[] args) {
		Map<String, String> inimiesed = new HashMap<>();
		inimiesed.put("12345678912", "Mati Karu");
		inimiesed.put("12345678913", "Kati Maru");
		inimiesed.put("12345678928", "Mummi Taru");
		System.out.println(inimiesed);

		System.out.println("kellel on iskukood 12345678928? " + inimiesed.get("12345678928"));

		for (String key : inimiesed.keySet()) {
			if (inimiesed.get(key).equals("Mummi Taru")) {
				System.out.println("Mummi taru isikukood on " + key);
			}
		}
		
		System.out.println("Kas Mummi Taru nimeline imine on loetelus? " + inimiesed.containsValue("Mummi Taru"));
	}
}
