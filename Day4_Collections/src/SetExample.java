import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetExample {

	public static void main(String args[]) {
		Set<String> nimed = new TreeSet<>();
		nimed.add("Mati");
		nimed.add("Kati");
		nimed.add("Mari");
		System.out.println(nimed);
		nimed.add("Mati");
		nimed.add("Jüri");
		System.out.println(nimed);
		for(String nimi: nimed) {
			System.out.println(nimi.hashCode());
		}
		nimed.remove("Mati");
		System.out.println(nimed);
		System.out.println("kas nimekirjas on Muri? - " + nimed.contains("Muri"));
		System.out.println("kas nimekirjas on Kati? - " + nimed.contains("Kati"));
		System.out.println("Setis on " + nimed.size() + " elementi");
	}
}
